<?php
/**
 * Defines the constants that are used to identify what to print within the
 * <body> of “index.php.”
 *
 * PHP version 5.3.28
 *
 * @category Default
 * @package  Default
 * @author   Roy Vanegas <roy@thecodeeducators.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://roy.vanegas.org Roy Vanegas
 */

define("EMAIL_FORM", 0);
define("DATABASE_QUERY_ERROR", -1);
define("DATABASE_CONNECTION_ERROR", -2);
