/*jslint browser: true, indent: 4, maxlen: 85 */

/* A global variable that will hold on to all the AJAX functionality. */
var xmlhttp;

/**
 * EMAILS MATCH
 */
function emails_match() {
    "use strict";

    var match = true,
        email1 = document.getElementById("email1"),
        email2 = document.getElementById("email2");

    if (email1.value === email2.value) {
        email1.style.backgroundColor = "#fff";
        email1.style.color = "#000";

        email2.style.backgroundColor = "#fff";
        email2.style.color = "#000";
    } else {
        email1.value = "Emails don’t match!";
        email1.style.backgroundColor = "#f00";

        email2.value = "Emails don’t match!";
        email2.style.backgroundColor = "#f00";

        match = false;
    }

    return match;
}

/**
 * INITIATE XML RESPONSE
 *
 * This function doesn’t return anything. It’s triggered by AJAX’s
 * onreadystatechange property.
 */
function initiate_xml_response() {
    "use strict";

    /*
        If all the data has been received and is complete (4) and status is ok
        (200), retrieve the AJAX call’s response.
    */
    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
        var xml_response = xmlhttp.responseText;

        if (xml_response === "Already exists") {
            document.getElementById("email1").style.backgroundColor
                = document.getElementById("email2").style.backgroundColor
                = "#f00";
        }

        // Write the response to both email fields.
        document.getElementById("email1").value
            = document.getElementById("email2").value = xml_response;
    }
}

/**
 * START AJAX COMMUNICATION
 */
function start_ajax_communication() {
    "use strict";

    /* If emails do not match, return before trying anything else. */
    if (!emails_match()) {
        return;
    }

    if (!xmlhttp) {
        xmlhttp = new XMLHttpRequest();
    }

    var url = "email_check.php?email=" +
        document.getElementById("email2").value;

    xmlhttp.open('GET', url, true);
    xmlhttp.onreadystatechange = initiate_xml_response;
    xmlhttp.send(null);
}

/**
 * MANAGE EVENT
 */
function manage_event(eventObj, event, eventHandler) {
    "use strict";

    // Attach the listeners to the email field — on standards browsers.
    if (eventObj.addEventListener) {
        eventObj.addEventListener(event, eventHandler, false);
    } else {
        // But this is how it’s done for that damned IE browser.
        if (eventObj.attachEvent) {
            event = "on" + event;
            eventObj.attachEvent(event, eventHandler);
        }
    }
}

/**
 * Once the window loads, trigger an anonymous function that attaches Ajax
 * functionality to the second email field when blurred.
 */
window.onload = function () {
    "use strict";

    var email = document.getElementById("email2");

    manage_event(email, "blur", start_ajax_communication);
};
